<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class ProductUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::user();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:20',
            'description' => 'required|max:500',
            'prices' => 'required|array|min:1',
            'prices.*.id' => 'required|exists:prices,id',
            'prices.*.value' => 'required|numeric',
            'prices.*.currency' => 'nullable|string|max:5',
            'newPrices' => 'nullable|array|min:1',
            'newPrices.*.value' => 'required|numeric',
            'newPrices.*.currency' => 'nullable|string|max:5',
            'pricesToDelete' => 'nullable|array',
            'pricesToDelete.*' => 'numeric|exists:prices,id',
        ];
    }
}
