<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductStoreRequest;
use App\Http\Requests\ProductUpdateRequest;
use App\Http\Services\ProductService;
use App\Models\Product;

class ProductController extends Controller
{
    protected $productService;

    public function __construct(ProductService $productService)
    {
        $this->productService = $productService;
    }

    public function index()
    {
        $products = $this->productService->productsList();

        return view('products.index', compact('products'));
    }

    public function create()
    {
        return view('products.create');
    }

    public function store(ProductStoreRequest $request)
    {
        $this->productService->saveProductData($request);

        return redirect('products')->with('success', 'Product has been created');
    }

    public function show(Product $product)
    {
        $product = $this->productService->productsDetails($product);

        return view('products.show', compact('product'));
    }

    public function edit(Product $product)
    {
        $product = $this->productService->productsDetails($product);

        return view('products.edit', compact('product'));
    }

    public function update(ProductUpdateRequest $request, Product $product)
    {
        $this->productService->updateProductData($request, $product);

        return redirect('products')->with('success', 'Product has been updated');
    }

    public function destroy(Product $product)
    {
        $this->productService->deleteProduct($product);

        return back()->with('success', 'Product has been deleted');
    }
}
