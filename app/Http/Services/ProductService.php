<?php

namespace App\Http\Services;

use App\Http\Requests\ProductStoreRequest;
use App\Http\Requests\ProductUpdateRequest;
use App\Models\Price;
use App\Models\Product;

class ProductService
{
    public function productsList()
    {
        return Product::select('id', 'name')->get();
    }

    public function saveProductData(ProductStoreRequest $request)
    {
        $data = $request->validated();
        $product = Product::create($data);
        foreach ($data['newPrices'] as $newPrice) {
            $this->savePrice($newPrice, $product->id);
        }
    }

    public function productsDetails(Product $product)
    {
        return Product::with('prices')->whereId($product->id)->first();
    }

    public function updateProductData(ProductUpdateRequest $request, Product $product)
    {
        $data = $request->validated();
        $product->update($data);
        if (isset($data['newPrices']))
            foreach ($data['newPrices'] as $newPrice) {
                $this->savePrice($newPrice, $product->id);
            }
        if (isset($data['prices']))
            foreach ($data['prices'] as $price) {
                $this->updatePrice($price, $product->id);
            }
        if (isset($data['pricesToDelete']))
            foreach ($data['pricesToDelete'] as $priceId) {
                $this->deletePrice($priceId, $product->id);
            }
    }

    public function deleteProduct(Product $product)
    {
        $product->delete();
    }

    private function savePrice(array $data, int $productId)
    {
        $data['product_id'] = $productId;
        Price::create($data);
    }

    private function updatePrice(array $data, int $productId)
    {
        $price = Price::where('id', $data['id'])->where('product_id', $productId);
        if ($price)
            $price->update($data);
    }

    private function deletePrice(int $priceId, int $productId)
    {
        $price = Price::where('id', $priceId)->where('product_id', $productId);
        if ($price)
            $price->delete();
    }
}