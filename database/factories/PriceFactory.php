<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Price;
use Faker\Generator as Faker;

$factory->define(Price::class, function (Faker $faker) {
    return [
        'product_id' => $faker->numberBetween(1, 10),
        'value' => $faker->randomNumber(2),
        'currency' => 'PLN',
    ];
});
