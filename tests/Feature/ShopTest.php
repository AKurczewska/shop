<?php

namespace Tests\Feature;

use App\Models\Price;
use App\Models\Product;
use App\Models\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ShopTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    public function testSignIn()
    {
        $user = factory(User::class)->make();
        $response = $this->actingAs($user)->get('/login');
        $response->assertRedirect('');
    }

    public function testCreateProductNotAuthenticated()
    {
        $data = [
            'name' => $this->faker->word,
            'description' => $this->faker->text,
            'newPrices' => array(array('value' => $this->faker->randomNumber(2))),
        ];

        $response = $this->json('POST', '/products', $data);
        $response->assertStatus(401);
        $response->assertJson(['message' => 'Unauthenticated.']);
    }

    public function testCreateProduct()
    {
        $data = [
            'name' => $this->faker->word,
            'description' => $this->faker->text,
            'newPrices' => array(array('value' => $this->faker->randomNumber(2))),
        ];
        $user = factory(User::class)->create();

        $response = $this->actingAs($user)->json('POST', '/products', $data);
        $response->assertStatus(302);
        $response->assertRedirect('/products');
        $response->assertSessionHas('success', 'Product has been created');

        $product = Product::with(['prices' => function ($price) use ($data) {
            $price->where('value', $data['newPrices'][0]['value']);
        }])->where('name', $data['name'])
            ->where('description', $data['description'])
            ->first();
        $this->assertNotNull($product);
    }

    function testCreateProductNotValid()
    {
        $user = factory(User::class)->create();
        $response = $this->actingAs($user)->json('POST', '/products', []);
        $response->assertStatus(422);
        $response->assertJson(['message' => 'The given data was invalid.']);
        $this->assertNull(Product::first());
    }

    public function testGettingAllProducts()
    {
        $product = factory(Product::class)->create();

        $response = $this->json('GET', '/products');
        $response->assertStatus(200);
        $response->assertSee($product->name);
    }

    public function testGettingProduct()
    {
        $product = factory(Product::class)->create();
        $price = Price::create([
            'product_id' => $product->id,
            'value' => $this->faker->randomNumber(2),
            'currency' => 'PLN',
        ]);

        $response = $this->json('GET', '/products/' . $product->id);
        $response->assertStatus(200);
        $response->assertSee($product->name)
            ->assertSee($product->description)
            ->assertSee($price->value)
            ->assertSee($price->currency);
    }

    public function testUpdateProductNotAuthenticated()
    {
        $product = factory(Product::class)->create();
        $price = Price::create([
            'product_id' => $product->id,
            'value' => $this->faker->randomNumber(2),
            'currency' => 'PLN',
        ]);

        $response = $this->json('PUT', '/products/' . $product->id, [
            'name' => 'Changed for test',
            'description' => $product->description,
            'prices' => array(array('id' => $price->id, 'value' => $price->value))
        ]);
        $response->assertStatus(401);
        $response->assertJson(['message' => 'Unauthenticated.']);
    }

    public function testUpdateProduct()
    {
        $product = factory(Product::class)->create();
        $price = Price::create([
            'product_id' => $product->id,
            'value' => $this->faker->randomNumber(2),
            'currency' => 'PLN',
        ]);

        $user = factory(User::class)->create();
        $response = $this->actingAs($user)->json('PUT', '/products/' . $product->id, [
            'name' => 'Changed for test',
            'description' => $product->description,
            'prices' => array(array('id' => $price->id, 'value' => $price->value))
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/products');
        $response->assertSessionHas('success', 'Product has been updated');

        $editedProduct = Product::where('name', 'Changed for test')
            ->where('id', $product->id)
            ->first();
        $this->assertNotNull($editedProduct);
    }

    function testUpdateProductNotValid()
    {
        $product = factory(Product::class)->create();

        $user = factory(User::class)->create();
        $response = $this->actingAs($user)->json('PUT', '/products/' . $product->id, []);
        $response->assertStatus(422);
        $response->assertJson(['message' => 'The given data was invalid.']);
    }

    public function testDeleteProductNotAuthenticated()
    {
        $product = factory(Product::class)->create();

        $response = $this->json('DELETE', '/products/' . $product->id);
        $response->assertStatus(401);
        $response->assertJson(['message' => 'Unauthenticated.']);
    }

    public function testDeleteProduct()
    {
        $product = factory(Product::class)->create();
        $price = Price::create([
            'product_id' => $product->id,
            'value' => $this->faker->randomNumber(2),
            'currency' => 'PLN',
        ]);

        $user = factory(User::class)->create();
        $response = $this->actingAs($user)->json('DELETE', '/products/' . $product->id);
        $response->assertStatus(302);
        $response->assertRedirect('');
        $response->assertSessionHas('success', 'Product has been deleted');

        $deletedProduct = Product::find($product->id);
        $deletedPrice = Price::find($price->id);
        $this->assertNull($deletedProduct);
        $this->assertNull($deletedPrice);
    }
}
