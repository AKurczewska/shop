<?php

namespace Tests\Unit;

use App\Models\Product;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProductTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    public function testCreateProduct()
    {
        $data = [
            'name' => $this->faker->word,
            'description' => $this->faker->text,
        ];
        $product = Product::create($data);

        $this->assertInstanceOf(Product::class, $product);
        $this->assertEquals($data['name'], $product->name);
        $this->assertEquals($data['description'], $product->description);
    }

    public function testFindProduct()
    {
        $product = factory(Product::class)->create();
        $found = Product::find($product->id);

        $this->assertInstanceOf(Product::class, $found);
        $this->assertEquals($found->name, $product->name);
        $this->assertEquals($found->description, $product->description);
    }

    public function testUpdateProduct()
    {
        $product = factory(Product::class)->create();
        $data = [
            'name' => $this->faker->word,
            'description' => $this->faker->text,
        ];
        $update = $product->update($data);

        $this->assertTrue($update);
        $this->assertEquals($data['name'], $product->name);
        $this->assertEquals($data['description'], $product->description);
    }

    public function testDeleteProduct()
    {
        $product = factory(Product::class)->create();

        $delete = $product->delete();

        $this->assertTrue($delete);
    }
}
