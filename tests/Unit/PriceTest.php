<?php

namespace Tests\Unit;

use App\Models\Price;
use App\Models\Product;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PriceTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    public function testCreatePrice()
    {
        $product = factory(Product::class)->create();
        $data = [
            'product_id' => $product->id,
            'value' => $this->faker->randomNumber(2),
            'currency' => 'PLN',
        ];
        $price = Price::create($data);

        $this->assertInstanceOf(Price::class, $price);
        $this->assertEquals($data['value'], $price->value);
        $this->assertEquals($data['currency'], $price->currency);
    }

    public function testFindPrice()
    {
        $product = factory(Product::class)->create();
        $data = [
            'product_id' => $product->id,
            'value' => $this->faker->randomNumber(2),
            'currency' => 'PLN',
        ];
        $price = Price::create($data);
        $found = Price::find($price->id);

        $this->assertInstanceOf(Price::class, $found);
        $this->assertEquals($found->value, $price->value);
        $this->assertEquals($found->currency, $price->currency);
    }

    public function testUpdatePrice()
    {
        $product = factory(Product::class)->create();
        $data = [
            'product_id' => $product->id,
            'value' => $this->faker->randomNumber(2),
            'currency' => 'PLN',
        ];
        $price = Price::create($data);

        $dataUpdate = [
            'value' => $this->faker->randomNumber(2),
            'currency' => 'PLN',
        ];

        $update = $price->update($dataUpdate);

        $this->assertTrue($update);
        $this->assertEquals($dataUpdate['value'], $price->value);
        $this->assertEquals($dataUpdate['currency'], $price->currency);
    }

    public function testDeletePrice()
    {
        $product = factory(Product::class)->create();
        $data = [
            'product_id' => $product->id,
            'value' => $this->faker->randomNumber(2),
            'currency' => 'PLN',
        ];
        $price = Price::create($data);

        $delete = $price->delete();

        $this->assertTrue($delete);
    }
}
