<form method="POST" action="{{ $action ?? '' }}" enctype="multipart/form-data" class="{{ !empty($disableFields) ? 'disabledForm' : '' }}">
    @if (!empty($method) && in_array($method, ["PUT", "PATCH", "DELETE"]) )
        {{ method_field($method) }}
    @endif
    {{ csrf_field() }}

    <div class="row">
        <div class="form-group col-md-3">
            <label for="name"><strong>Name:</strong></label>
            <input
                type="text"
                class="form-control"
                name="name"
                value="{{  old('name') ?? (isset($product) ? $product->name : null) }}"
                required />
        </div>
        <div class="form-group col-md-9">
            <label for="description"><strong>Description:</strong></label>
            <textarea
                rows="2"
                cols="50"
                class="form-control"
                name="description"
                required>{{  old('description') ?? (isset($product) ? $product->description : null) }}
            </textarea>
        </div>
    </div>

    <div class="form-group">
        <label><strong>Prices:</strong></label>
        @if (empty($disableFields))
            <div class="form-group">
                <button type="button" name="add" id="add" class="btn btn-outline-primary">Add price</button>
            </div>
        @endif
        <div id="dynamic">
            @if(isset($product))
                <div id="delete_price" type="hidden"></div>
                @foreach ($product->prices as $price)
                    <div id="old_price{{$price->id}}" class="row">
                        <input type="hidden" name="prices[{{$loop->index}}][id]" value="{{$price->id}}">
                        <div class="form-group col-md-3">
                            <input
                                type="number"
                                step="0.01"
                                min="0"
                                class="form-control"
                                name="prices[{{$loop->index}}][value]"
                                placeholder="value"
                                value={{ $price->value }}
                                required />
                        </div>
                        <div class="form-group col-md-2">
                            <input
                                type="text"
                                class="form-control"
                                name="prices[{{$loop->index}}][currency]"
                                placeholder="currency"
                                value={{ $price->currency }} />
                        </div>
                        @if (empty($disableFields))
                            <div class="form-group col-md-2">
                                <button type="button" name="remove" id="{{$price->id}}" class="btn btn-danger old-price-remove">X</button>
                            </div>
                        @endif
                    </div>
                @endforeach
            @else
                <div id="price0" class="row">
                    <div class="form-group col-md-3">
                        <input
                            type="number"
                            step="0.01"
                            min="0"
                            class="form-control"
                            name="newPrices[0][value]"
                            placeholder="value"
                            required />
                    </div>
                    <div class="form-group col-md-2">
                        <input
                            type="text"
                            class="form-control"
                            name="newPrices[0][currency]"
                            placeholder="currency" />
                    </div>
                    @if (empty($disableFields))
                        <div class="form-group col-md-2">
                            <button type="button" name="remove" id="0" class="btn btn-danger price-remove">X</button>
                        </div>
                    @endif
                </div>
            @endif
        </div>

    </div>

    @if (empty($disableFields))
        <div class="row">
            <div class="form-group col-md-12">
                <button type="submit" name="submit" value="Submit" class="btn btn-outline-success">Submit</button>
            </div>
        </div>
    @endif
</form>
