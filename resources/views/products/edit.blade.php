@extends('layouts.app')
@section('content')

<div class="container">
    <div class="form-header">
        <h2>Edit product</h2>
        <div class="links">
            <a href="/products" class="btn btn-outline-danger">Cancel</a>
        </div>
    </div>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div><br />
    @endif
    @include('products._form', [
        'action' => url('products', $product),
        'method' => 'PUT'
    ])
</div>

@endsection
