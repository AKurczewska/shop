@extends('layouts.app')
@section('title', 'VFL Audits')
@section('content')

<div class="container">
    @if (Session::has('success'))
        <div class="alert alert-success">
            <p>{{ \Session::get('success') }}</p>
        </div>
    @elseif (Session::has('error'))
        <div class="alert alert-danger">
            <p>{{ Session::get('error') }}</p>
        </div>
    @endif
    <div class="form-header">
        <h2>Products</h2>
        @auth
            <div class="links">
                <a href="products/create" class="btn btn-outline-primary">
                    Create
                </a>
            </div>
        @endauth
    </div>
    <div class="row">
        <div class="form-group col-md-3">
            <input id="search" class="form-control" type="text" placeholder="Search..">
        </div>
    </div>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-sm">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody id="products_table">
                @if (count($products))
                    @foreach ($products as $product)
                        <tr>
                            <td>{{$loop->index + 1}}</td>
                            <td>{{$product->name}}</td>
                            <td>
                                <div class="row action-buttons">
                                    <a href="{{action('ProductController@show', $product->id)}}" class="btn btn-outline-primary">Details</a>
                                    @auth
                                        <a href="{{action('ProductController@edit', $product->id)}}" class="btn btn-outline-warning">Edit</a>
                                        <form action="{{action('ProductController@destroy', $product->id)}}" method="post">
                                            {{ csrf_field() }}
                                            <input name="_method" type="hidden" value="DELETE">
                                            <button class="btn btn-outline-danger confirm-delete" type="submit">Delete</button>
                                        </form>
                                    @endauth
                                </div>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr><td colspan="3" class="empty-table">no data</td></tr>
                @endif
            </tbody>
        </table>
   </div>
</div>

@endsection
