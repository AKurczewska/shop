@extends('layouts.app')
@section('content')

<div class="container">
    <div class="form-header">
        <h2>Product</h2>
        <div class="links">
            <a href="/products" class="btn btn-outline-danger">Go back</a>
        </div>
    </div>
    @include('products._form', [
        'action' => url('products'),
        'method' => 'POST',
        'disableFields' => true
    ])
</div>

@endsection
