$(function () {
    $('.confirm-delete').on('click', function () {
        return confirm('Are you sure you want to delete this item?');
    });

    var i = 1;
    $('#add').click(function () {
        $('#dynamic').append(
            '<div id="price' + i + '" class="row">' +
            '<div class="form-group col-md-3">' +
            '<input type="number" step="0.01" min="0" class="form-control" name="newPrices[' + i + '][value]" placeholder="value" required/>' +
            '</div>' +
            '<div class="form-group col-md-2">' +
            '<input type="text" class="form-control" name="newPrices[' + i + '][currency]" placeholder="currency"/>' +
            '</div>' +
            '<div class="form-group col-md-2">' +
            '<button type="button" name="remove" id="' + i + '" class="btn btn-danger price-remove">X</button>' +
            '</div>' +
            '</div>');
        i++;
    });

    $(document).on('click', '.price-remove', function () {
        var button_id = $(this).attr("id");
        if ($('.price-remove').length + $('.old-price-remove').length == 1)
            alert("You can't delete all prices!");
        else
            $('#price' + button_id).remove();
    });

    var j = 0;
    $(document).on('click', '.old-price-remove', function () {
        var button_id = $(this).attr("id");
        if ($('.price-remove').length + $('.old-price-remove').length == 1)
            alert("You can't delete all prices!");
        else {
            $('#old_price' + button_id).remove();
            $('#delete_price').append(
                '<input type="hidden" name="pricesToDelete[' + j + ']" value="' + button_id + '"/>'
            );
            j++;
        }
    });

    $(function () {
        $('.disabledForm').find('input, textarea').attr('disabled', 'disabled');
    });
});
