<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes([
    'register' => false,
    'reset' => false
    ]);

Route::group([
    'middleware' => ['auth'],
    'prefix' => 'products'
], function () {
    Route::get('create', 'ProductController@create');
    Route::post('', 'ProductController@store');
    Route::get('{product}/edit', 'ProductController@edit');
    Route::put('{product}', 'ProductController@update');
    Route::delete('{product}', 'ProductController@destroy');
});

Route::get('/', 'ProductController@index');
Route::get('products', 'ProductController@index');
Route::get('products/{product}', 'ProductController@show');
